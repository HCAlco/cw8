<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategoriesData extends Fixture
{
    public const CATEGORY_ONE = 'Fantasy';
    public const CATEGORY_TWO = 'Magic Realism';
    public const CATEGORY_THREE = 'Historical';
    public const CATEGORY_FOUR = 'Adventure';
    public const CATEGORY_FIVE = 'Criminal';
    public const CATEGORY_SIX = 'Detective';
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setCategory('Фэнтези');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setCategory('Магический реализм');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setCategory('История');
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setCategory('Приключение');
        $manager->persist($category4);

        $category5 = new Category();
        $category5->setCategory('Криминал');
        $manager->persist($category5);

        $category6 = new Category();
        $category6->setCategory('Детектив');
        $manager->persist($category6);
        $manager->flush();

        $this->addReference(self::CATEGORY_ONE, $category1);
        $this->addReference(self::CATEGORY_TWO, $category2);
        $this->addReference(self::CATEGORY_THREE, $category3);
        $this->addReference(self::CATEGORY_FOUR, $category4);
        $this->addReference(self::CATEGORY_FIVE, $category5);
        $this->addReference(self::CATEGORY_SIX, $category6);

    }

}
