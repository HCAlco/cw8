<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBooksData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_ONE);
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_TWO);
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_THREE);
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_FOUR);
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_FIVE);
        $categories[] = $this->getReference(LoadCategoriesData::CATEGORY_SIX);
        $titles = ['Гарри Поттер и узник Азкабана', 'Зеленая миля', 'Унесенные ветром',
                    'Властелин колец', 'Крестный отец', 'Собака Баскервилей'];
        $authors = ['Джоан Роулинг', 'Стивен Кинг', 'Маргарет Митчелл', 'Джон Роналд Руэл Толкин',
                    'Марио Пьюзо', 'Артур Конан Дойл'];
        for($i = 0; $i < count($titles); $i++){
            $book = new Book;
            $book->setTitle($titles[$i])
                ->setAuthor($authors[$i])
                ->addCategories($categories[$i])
                ->setImage('image'.$i.'.jpg');
            $manager->persist($book);
        }
        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            LoadCategoriesData::class,
        );
    }

}
