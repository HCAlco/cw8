<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Reader;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadReadersData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new Reader();
        $user
            ->setAddress('Улица Домоседова 137/3')
            ->setPassport('2345U64353456')
            ->setTicket(md5('2345U64353456'))
            ->setNameInfo('Асад Асадович Асадов');
        $manager->persist($user);

        $user1 = new Reader();
        $user1
            ->setAddress('Улица Пушкина 210/1')
            ->setPassport('8654328435124')
            ->setTicket(md5('8654328435124'))
            ->setNameInfo('Лосев Евгений Петрович');
        $manager->persist($user1);

        $manager->flush();
    }

}
