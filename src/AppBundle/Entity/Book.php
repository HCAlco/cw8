<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 */
class Book
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=127)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=127, nullable=false)
     */
    private $author;


    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text", nullable=true, unique=false)
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="image_file", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;


    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="books")
     */
    private $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Reader", inversedBy="books")
     */
    private $readers;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Act", mappedBy="book")
     */
    private $acts;

    public function __construct()
    {
        $this->readers = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->acts = new ArrayCollection();
    }



    public function bookContains()
    {
         $this->acts->exists($p);
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Book
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Book
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return $this->image ?: '';
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param $categories
     * @return Book
     */
    public function addCategories($categories)
    {
        $this->categories->add($categories);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReaders()
    {
        return $this->readers;
    }

    /**
     * @param Reader $reader
     */
    public function addReader($reader)
    {
        $this->readers->add($reader);
    }

    /**
     * @return ArrayCollection
     */
    public function getActs(): ArrayCollection
    {
        return $this->acts;
    }

    /**
     * @param $act
     * @return Book
     */
    public function addAct($act)
    {
        $this->acts->add($act);
        return $this;
    }
}

