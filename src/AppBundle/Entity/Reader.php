<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_info", type="string", length=255)
     */
    private $nameInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="string", length=255)
     */
    private $passport;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket", type="string", length=255)
     */
    private $ticket;

    /**
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="readers")
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameInfo
     *
     * @param string $nameInfo
     *
     * @return Reader
     */
    public function setNameInfo($nameInfo)
    {
        $this->nameInfo = $nameInfo;

        return $this;
    }

    /**
     * Get nameInfo
     *
     * @return string
     */
    public function getNameInfo()
    {
        return $this->nameInfo;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Reader
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return Reader
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @return mixed
     */
    public function getBooks()
    {
        return $this->books;
    }

    /**
     * @param $book
     * @return Reader
     */
    public function addBook($book)
    {
        $this->books->add($book);
        return $this;
    }
}

