<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Act
 *
 * @ORM\Table(name="act")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActRepository")
 */
class Act
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket", type="string", length=255)
     */
    private $ticket;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="return_date", type="datetime", nullable=false)
     */
    private $returnDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="real_return_date", type="datetime", nullable=true)
     */
    private $realReturnDate;

    /**
     * @ORM\ManyToOne(targetEntity="Book", inversedBy="acts")
     */
    private $book;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return Act
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set book
     *
     * @param string $book
     *
     * @return Act
     */
    public function setBook($book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return string
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * Set returnDate
     *
     * @param \DateTime $returnDate
     *
     * @return Act
     */
    public function setReturnDate(\DateTime $returnDate)
    {
        $this->returnDate = $returnDate;

        return $this;
    }

    /**
     * Get returnDate
     *
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @return string
     */
    public function getPrintableReturnDate()
    {
        return $this->returnDate->format('Y-m-d h-m-s');
    }

    /**
     * @return DateTime
     */
    public function getRealReturnDate()
    {
        return $this->realReturnDate;
    }

    /**
     * @return string
     */
    public function getPrintableRealReturnDate()
    {
        return $this->realReturnDate->format('Y-m-d h-m-s');
    }

    /**
     * @param DateTime $realReturnDate
     * @return Act
     */
    public function setRealReturnDate($realReturnDate): Act
    {
        $this->realReturnDate = $realReturnDate;
        return $this;
    }
}

