<?php

namespace AppBundle\Repository;

/**
 * ActRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ActRepository extends \Doctrine\ORM\EntityRepository
{
    public function findActsByTicket($ticket){
        return $this->createQueryBuilder('a')
            ->where('a.ticket = :ticket')
            ->setParameter('ticket', $ticket)
            ->getQuery()
            ->getResult();

    }
}
