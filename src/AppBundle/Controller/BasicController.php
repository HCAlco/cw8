<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Act;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $books = $this->getDoctrine()->getRepository('AppBundle:Book')->findAll();
        return $this->render('@App/Basic/index.html.twig', array(
            'books' => $books,
            'categories' => $categories
        ));
    }

    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function languageAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/books/{id}/get")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function bookingAction(Request $request, int $id)
    {
        $act = new Act();
        $book = $this->getDoctrine()->getRepository('AppBundle:Book')->find($id);
        dump($book);

        $form_builder = $this->createFormBuilder($act);
        $form_builder->add('ticket', TextType::class, array('label' => 'Читательский билет'));
//        $form_builder->add('address', TextType::class, array('label' => 'Адрес'));
        $form_builder->add('returnDate', DateTimeType::class, array('label' => 'Время возврата'));
        $form_builder->add('save', SubmitType::class, array('label' => 'Добавить'));
        $form = $form_builder->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $act = $form->getData();
            $act->setBook($book);

            $em = $this->getDoctrine()->getManager();
            $em->persist($act);
            $em->flush();

            return $this->redirectToRoute('app_basic_index');
        }
        return $this->render('@App/Basic/booking.html.twig', array(
            'form' => $form->createView(),
            'book' => $book
        ));
    }

    /**
     * @Route("/category/{id}")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryAction(int $id)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);
        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        $books = $category->getBooks();
        return $this->render('@App/Basic/index.html.twig', array(
            'books' => $books,
            'element' => $category,
            'categories' => $categories
        ));
    }

}
