<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class ReaderController extends Controller
{
    /**
     * @Route("/register/")
     * @Method({"GET", "PUT", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $reader = new Reader();

        $form_builder = $this->createFormBuilder($reader);
        $form_builder->add('nameInfo', TextType::class, array('label' => 'ФИО'));
        $form_builder->add('address', TextType::class, array('label' => 'Адрес'));
        $form_builder->add('passport', TextType::class, array('label' => 'ID Пасспорта'));
        $form_builder->add('save', SubmitType::class, array('label' => 'Добавить'));
        $form = $form_builder->getForm();

        $form->handleRequest($request);
        dump($form);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $reader = $form->getData();
                $ticket = md5($reader->getPassport());
                $reader->setTicket($ticket);
            } catch (\Exception $e) {
                echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($reader);
            $em->flush();

            return $this->render('@App/Reader/congrats.html.twig', array(
                'ticket' => $ticket
            ));
        }
        return $this->render('@App/Reader/register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/mybooks")
     */
    public function myBooksAction()
    {
        return $this->render('@App/Reader/myBooks.html.twig', array(

        ));
    }

    /**
     * @Route("/view")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request)
    {
        $ticket = $request->get('ticket');
        $acts = $this->getDoctrine()
            ->getRepository('AppBundle:Act')
            ->findActsByTicket($ticket);

        return $this->render('@App/Reader/view.html.twig', array(
            'acts' => $acts
        ));
    }

}
